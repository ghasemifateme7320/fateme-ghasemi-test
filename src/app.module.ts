import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MailModule } from './services/email/email.module';
import { UserEventModule } from './services/user-event/user-event.module';

@Module({
  imports: [
    UserEventModule,
    MailModule,
    MongooseModule.forRoot('mongodb://root:example@mongo:27017/'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}