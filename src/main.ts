import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const configService = new ConfigService();

  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('assignment')
    .setDescription('The assignment API description')
    .setVersion('1.0')
    .addTag('assignment')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);
  await app.listen(configService.get('PORT', 3000));
  return app;
}

bootstrap().then((app) => {
  console.log(`Server is running on port 3000`);
});
