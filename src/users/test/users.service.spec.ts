// users/test/users.service.spec.ts

import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { UsersService } from '../users.service';
import { User } from '../models/user.schema';

describe('UsersService', () => {
  let service: UsersService;
  const mockUserModel = {
    create: jest.fn(),
    find: jest.fn(),
    findById: jest.fn(),
    findByIdAndDelete: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getModelToken('User'),
          useValue: mockUserModel,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });



  describe('findAll', () => {
    it('should return all users', async () => {
      const mockUsers = [{ name: 'John', email: 'john@example.com' }];
      mockUserModel.find.mockResolvedValue(mockUsers);

      const result = await service.findAll();

      expect(result).toEqual(mockUsers);
      expect(mockUserModel.find).toHaveBeenCalled();
    });
  });

  describe('findOneById', () => {
    it('should return a user by ID', async () => {
      const mockUserId = '123456';
      const mockUser = { _id: mockUserId, name: 'John', email: 'john@example.com' } as User;
      mockUserModel.findById.mockResolvedValue(mockUser);

      const result = await service.findOneById(mockUserId);

      expect(result).toEqual(mockUser);
      expect(mockUserModel.findById).toHaveBeenCalledWith(mockUserId);
    });
  });

  describe('deleteOne', () => {
    it('should delete a user by ID', async () => {
      const mockUserId = '123456';
      const mockUser = { _id: mockUserId, name: 'John', email: 'john@example.com' } as User;
      mockUserModel.findByIdAndDelete.mockResolvedValue(mockUser);

      const result = await service.deleteOne(mockUserId);

      expect(result).toEqual(mockUser);
      expect(mockUserModel.findByIdAndDelete).toHaveBeenCalledWith(mockUserId);
    });
  });
});
