// users/users.service.ts

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './models/user.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User')
    private readonly userModel: any,
  ) {}

  async create(userDto: any): Promise<User> {
    const createdUser = new this.userModel(userDto);
    return createdUser.save();
  }

  async findAll(): Promise<User[]> {
    return this.userModel.find();
  }

  async findOneById(userId: string): Promise<User> {
    return this.userModel.findById(userId);
  }

  async deleteOne(userId: string): Promise<User> {
    return this.userModel.findByIdAndDelete(userId);
  }
}
