import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User, UserSchema } from './models/user.schema';
import { UserEventModule } from '../services/user-event/user-event.module';
import { MailModule } from '../services/email/email.module';
import { UserEventPublisher } from 'src/services/user-event/user-event.publisher';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    UserEventModule,
    MailModule,
  ],
  controllers: [UsersController],
  providers: [UsersService, UserEventPublisher],
  exports: [UsersService],
})
export class UsersModule {}
