import { Module } from '@nestjs/common';
import { MailService } from './email.service';
import { EmailModuleConf } from './config';

@Module({
  imports: [EmailModuleConf],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}
