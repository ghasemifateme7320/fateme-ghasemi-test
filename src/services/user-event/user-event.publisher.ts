
  import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
  import { Injectable } from '@nestjs/common';
  
  @Injectable()
  export class UserEventPublisher {

  
    constructor(private readonly amqpConnection: AmqpConnection) {}
  
    async publish<T>(data: any): Promise<void> {
      try {
        await this.amqpConnection.publish(
          "USER_EXCHANGE_NAME",
          "USER_ROUTING_KEY",
          data,
          { persistent: true, correlationId: data.request_track_id },
        );
      } catch (err) {
        // todo sentry
      }
    }
  }
  