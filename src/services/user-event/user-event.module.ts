import { Module } from '@nestjs/common';
import { UserEventPublisher } from './user-event.publisher';
import { RabbitMQBrokerModule } from './rabbit/rabbitmq.module';

@Module({
  imports: [RabbitMQBrokerModule],
  providers: [UserEventPublisher],
  exports: [UserEventPublisher],
})
export class UserEventModule {}
