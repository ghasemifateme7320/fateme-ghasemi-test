import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import {
  MessageHandlerErrorBehavior,
  RabbitMQConfig,
  RabbitMQModule,
} from '@golevelup/nestjs-rabbitmq';

import { getExchangesList, getRabbitMqURIs } from './config';

/**
 * For working with rabbitmq
 *
 * Because application need to use exchanges to connect to
 * rabbitmq and now rabbitmq transport of nestjs microservices did not
 * support it, I use @golevelup/nestjs-rabbitmq package.
 */
@Module({
  imports: [
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService): RabbitMQConfig => ({
        uri: getRabbitMqURIs(configService),
        exchanges: getExchangesList(configService),
        connectionInitOptions: {
          wait: false,
        },
        enableControllerDiscovery: true,
        defaultSubscribeErrorBehavior: MessageHandlerErrorBehavior.REQUEUE,
        prefetchCount: 20,
        deserializer(bufferMessage) {
          const msg = bufferMessage.toString();
          try {
            return JSON.parse(msg);
          } catch (error) {
            return msg;
          }
        },
      }),
    }),
  ],
  exports: [RabbitMQModule],
})
export class RabbitMQBrokerModule {}
