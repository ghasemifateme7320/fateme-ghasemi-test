import { RabbitMQExchangeConfig } from '@golevelup/nestjs-rabbitmq';
import { ConfigService } from '@nestjs/config';

export const getRabbitMqURIs = (configService: ConfigService) => {
  const rabbitmqURI = `amqp://${configService.get(
    'RABBITMQ_USER',
  )}:${configService.get('RABBITMQ_PASSWORD')}@${configService.get(
    'RABBITMQ_HOST',
  )}:${configService.get('RABBITMQ_PORT')}${configService.get(
    'RABBITMQ_VHOST',
  )}`;
  return rabbitmqURI;
};

export const getExchangesList = (configService: ConfigService): RabbitMQExchangeConfig[] => {
  const exchangeList: RabbitMQExchangeConfig[] = [
    {
      name: configService.get('RABBITMQ_EXCHANGE_NAMES'),
      type: 'topic',
      createExchangeIfNotExists: true,
      options: {
        durable: true,
      },
    },
  ];
  return exchangeList;
};
